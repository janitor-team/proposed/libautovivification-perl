Source: libautovivification-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ivan Kohler <ivan-debian@420.am>,
           gregor herrmann <gregoa@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12),
               perl-xs-dev,
               perl:native
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libautovivification-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libautovivification-perl.git
Homepage: https://metacpan.org/release/autovivification

Package: libautovivification-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Description: pragma for lexically disabling autovivification
 autovivication is a Perl pragma that enables developers to control whether
 variables can be created automatically on their first use, rather than
 requiring them to be defined beforehand. While occasionally useful, this
 behaviour can result in subtle bugs that are difficult to debug.
 .
 This pragma lets you disable autovivification for some constructs and can
 optionally also emit a warning or error when it would have happened.
